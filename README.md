# Japh
PHP fullstack framework. GPL-3.0-or-later

# Installation
Just clone this repository, and install packages :

```
$ composer install
$ npm install
```
